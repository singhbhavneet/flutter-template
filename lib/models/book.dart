import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'book.g.dart';

@JsonSerializable(explicitToJson: true)
class Book extends Equatable {
  final String id;
  final String title;
  final String description;
  final String thumbnail;

  Book(this.id, this.title, this.description, this.thumbnail);

  Book.named({this.id, this.title, this.description, this.thumbnail});

  Book copyWith(
      {String id, String title, String description, String thumbnail}) {
    return Book.named(
        id: id ?? this.id,
        title: title ?? this.title,
        description: description ?? this.description,
        thumbnail: thumbnail ?? this.thumbnail);
  }

  @override
  List<Object> get props => [id];

  Map<String, dynamic> toJson() => _$BookToJson(this);

  factory Book.fromJson(json) => _$BookFromJson(json);
}
