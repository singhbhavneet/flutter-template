import 'package:flutter/material.dart';
import 'package:template/models/book.dart';

class BookWidget extends StatelessWidget {
  final Book book;
  final Function(Book book) onTap;
  const BookWidget({Key key, this.book, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      double width = constraints.biggest.width;
      double thumbnailWidth = width * 0.25;
      return InkWell(
        onTap: onTap == null ? null : () => onTap(book),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: thumbnailWidth,
                height: thumbnailWidth * 1.4,
                decoration: BoxDecoration(
                    color: Color(0xffeeeeee),
                    borderRadius: BorderRadius.circular(12),
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage("${book.thumbnail}"))),
              ),
              SizedBox(
                width: 18,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      "${book.title}",
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      "${book.description}",
                      style: TextStyle(
                        fontSize: 14,
                      ),
                      maxLines: 5,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}
