import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ocg_app/ocg_app.dart';
import 'package:template/blocs/book_add.dart';
import 'package:template/blocs/book_list.dart';
import 'package:template/repository/book.dart';
import 'package:template/screens/book.dart';
import 'package:template/screens/book_add.dart';
import 'package:template/screens/home.dart';
import 'package:template/screens/splash.dart';
import 'package:template/utils/routes.dart';

void main() {
  runApp(MyApp());
}

bool isRegisterd = false;

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BookRepository bookRepository = BookTestRepository();
    if (!isRegisterd) {
      ServiceRegister.registerService<BookRepository>(bookRepository);
      isRegisterd = true;
    }
    return MultiBlocProvider(
      providers: [
        BlocProvider<BookListBloc>(
          create: (context) => BookListBloc(bookRepository),
        ),
        BlocProvider<BookAddBloc>(
          create: (context) => BookAddBloc(bookRepository),
        )
      ],
      child: MaterialApp(
        title: 'Books',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          appBarTheme: AppBarTheme(
              centerTitle: true,
              brightness: Brightness.light,
              elevation: 2,
              color: Colors.white,
              iconTheme: IconThemeData(color: Colors.black),
              textTheme: TextTheme(
                  headline6: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w600))),
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        routes: {
          Routes.splash: (context) => SplashScreen(),
          Routes.home: (context) => HomeScreen(),
          Routes.book: (context) => BookScreen(),
          Routes.bookAdd: (context) => BookAddScreen(),
        },
      ),
    );
  }
}
