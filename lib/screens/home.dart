import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ocg_app/ocg_app.dart';
import 'package:template/blocs/book_list.dart';
import 'package:template/models/book.dart';
import 'package:template/utils/routes.dart';
import 'package:template/widgets/book.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<BookListBloc>(context).add(BookListRefreshEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.all(12.0),
          child: FlutterLogo(),
        ),
        title: Text("Books"),
      ),
      body: BlocBuilder<BookListBloc, BookListState>(
        builder: (context, state) {
          if (state is BookListLoadedState) {
            var books = state.books;
            return RefreshIndicator(
              onRefresh: () async {
                BlocProvider.of<BookListBloc>(context)
                    .add(BookListRefreshEvent());
                return;
              },
              child: ListView.builder(
                itemBuilder: (context, index) => BookWidget(
                  book: books[index],
                  onTap: _onTap,
                ),
                itemCount: books.length,
              ),
            );
          }
          if (state is BookListLoadingState) {
            return Center(
              child: AppProgressIndicator(),
            );
          }
          if (state is BookListErrorState) {
            return Center(
              child: AppErrorWidget(error: "${state.error}"),
            );
          }
          return Container();
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).pushNamed(Routes.bookAdd);
        },
      ),
    );
  }

  void _onTap(Book book) {
    Navigator.of(context)
        .pushNamed(Routes.book, arguments: BookRouteData(book));
  }
}
