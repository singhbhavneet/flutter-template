import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ocg_app/ocg_app.dart';
import 'package:template/utils/routes.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    _moveForward();
  }

  void _moveForward() async {
    await _controller.forward();
    await Future.delayed(Duration(milliseconds: 250));
    Navigator.of(context).pushNamed(Routes.home);
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark
          .copyWith(statusBarColor: Colors.transparent),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: LayoutBuilder(builder: (context, constraints) {
          double width = constraints.biggest.width;
          return SizedBox(
            child: SplashWidget(
              child: SizedBox(
                  child: FlutterLogo(
                size: width / 2,
              )),
              controller: _controller,
            ),
          );
        }),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
