import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:ocg_app/ocg_app.dart';
import 'package:template/blocs/book_add.dart';
import 'package:template/blocs/book_list.dart';
import 'package:template/models/book.dart';

class BookAddScreen extends StatefulWidget {
  BookAddScreen({Key key}) : super(key: key);

  @override
  _BookAddScreenState createState() => _BookAddScreenState();
}

class _BookAddScreenState extends State<BookAddScreen> {
  final GlobalKey<FormState> key = GlobalKey<FormState>();
  Book _book;

  @override
  void initState() {
    super.initState();
    _book = Book.named();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<BookAddBloc, BookAddState>(
      listener: (context, state) {
        if (state is BookAddSuccessState) {
          BlocProvider.of<BookListBloc>(context).add(BookListRefreshEvent());
          Navigator.of(context).pop();
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("Add Book"),
        ),
        body: BlocBuilder<BookAddBloc, BookAddState>(
          buildWhen: (previous, current) => current is BookAddLoadingState,
          builder: (context, state) => ModalProgressHUD(
            inAsyncCall: state is BookAddLoadingState,
            child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: 18, vertical: 12),
                child: Form(
                  key: key,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextFormField(
                        decoration: InputDecoration(labelText: "Title"),
                        validator: (value) =>
                            InputHelper.validateMessage("Title", value),
                        onChanged: (value) {
                          _book = _book.copyWith(title: value);
                        },
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      TextFormField(
                        decoration: InputDecoration(labelText: "Description"),
                        validator: (value) =>
                            InputHelper.validateMessage("Description", value),
                        onChanged: (value) {
                          _book = _book.copyWith(description: value);
                        },
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      TextFormField(
                        decoration: InputDecoration(labelText: "Thumbnail"),
                        validator: (value) =>
                            InputHelper.validateMessage("Thumbnail", value),
                        onChanged: (value) {
                          _book = _book.copyWith(thumbnail: value);
                        },
                      ),
                      SizedBox(
                        height: 32,
                      ),
                      RoundedButton(
                        "Submit",
                        onTap: _onSubmit,
                        borderRadius: 8,
                        color: Theme.of(context).primaryColor,
                      )
                    ],
                  ),
                )),
          ),
        ),
      ),
    );
  }

  void _onSubmit() async {
    if (!key.currentState.validate()) return;
    BlocProvider.of<BookAddBloc>(context).add(BookAddStartEvent(_book));
  }
}
