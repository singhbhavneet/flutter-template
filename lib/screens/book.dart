import 'package:flutter/material.dart';
import 'package:template/models/book.dart';
import 'package:template/utils/routes.dart';

class BookScreen extends StatefulWidget {
  BookScreen({Key key}) : super(key: key);

  @override
  _BookScreenState createState() => _BookScreenState();
}

class _BookScreenState extends State<BookScreen> {
  Book _book;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      var arguments = ModalRoute.of(context).settings.arguments;
      if (arguments is BookRouteData) {
        setState(() {
          _book = arguments.book;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Book"),
      ),
      body: _book == null
          ? Container()
          : LayoutBuilder(builder: (context, constraints) {
              double width = constraints.biggest.width;
              double thumbnailWidth = width * 0.4;
              return SingleChildScrollView(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: thumbnailWidth,
                      height: thumbnailWidth * 1.4,
                      decoration: BoxDecoration(
                          color: Color(0xffeeeeee),
                          borderRadius: BorderRadius.circular(12),
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage("${_book.thumbnail}"))),
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    Column(
                      children: [
                        Text(
                          "${_book.title}",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w700),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Text(
                          "${_book.description}",
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              );
            }),
    );
  }
}
