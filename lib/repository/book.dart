import 'package:ocg_app/ocg_app.dart';
import 'package:template/models/book.dart';

abstract class BookRepository extends Crud<Book> {}

///
/// Test
///
class BookTestRepository extends BookRepository {
  static List<Book> _books = [];

  @override
  Future<AppResponse<String>> create(Book object) async {
    try {
      assert(object.title != null && object.description != null);
      var book = object.copyWith(id: IDHelper().generateId());
      _books.add(book);
      return AppResponse.named(data: book.id);
    } catch (e) {
      return AppResponse.named(error: "$e");
    }
  }

  @override
  Future<AppResponse<bool>> delete(String id) async {
    int index = _books.indexWhere((element) => element.id == id);
    if (index >= 0) {
      _books.removeAt(index);
      return AppResponse.named(data: true);
    }
    return AppResponse.named(error: "Not Found");
  }

  @override
  Future<AppResponse<List<Book>>> getList(
      {int page = 1, int count = 10}) async {
    assert(page > 0);
    await Future.delayed(Duration(milliseconds: 750));
    return AppResponse.named(
        data: _books.skip((page - 1) * count).take(count).toList());
  }

  @override
  Future<AppResponse<Book>> read(String id) async {
    int index = _books.indexWhere((element) => element.id == id);
    if (index >= 0) {
      return AppResponse.named(data: _books[index]);
    }
    return AppResponse.named(error: "Not Found");
  }

  @override
  Future<AppResponse<bool>> update(Book object) async {
    int index = _books.indexWhere((element) => element.id == object.id);
    if (index >= 0) {
      _books[index] = object;
      return AppResponse.named(data: true);
    }
    return AppResponse.named(error: "Not Found");
  }
}
