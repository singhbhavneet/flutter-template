import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ocg_app/ocg_app.dart';
import 'package:template/models/book.dart';
import 'package:template/repository/book.dart';

abstract class BookAddEvent {}

class BookAddStartEvent extends BookAddEvent {
  final Book book;
  BookAddStartEvent(this.book);
}

abstract class BookAddState {}

class BookAddIdleState extends BookAddState {}

class BookAddLoadingState extends BookAddState {}

class BookAddSuccessState extends BookAddState {
  BookAddSuccessState();
}

class BookAddErrorState extends BookAddState {
  final String error;
  BookAddErrorState(this.error);
}

class BookAddBloc extends Bloc<BookAddEvent, BookAddState> {
  final BookRepository bookRepository;
  BookAddBloc(this.bookRepository) : super(BookAddIdleState());
  @override
  Stream<BookAddState> mapEventToState(BookAddEvent event) async* {
    if (event is BookAddStartEvent) {
      yield BookAddLoadingState();
      AppResponse<String> response = await bookRepository.create(event.book);
      if (response.isSuccess) {
        yield BookAddSuccessState();
      } else {
        yield BookAddErrorState(response.error);
      }
    }
  }
}
