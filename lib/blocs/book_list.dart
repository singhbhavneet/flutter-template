import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ocg_app/ocg_app.dart';
import 'package:template/models/book.dart';
import 'package:template/repository/book.dart';

abstract class BookListEvent {}

class BookListRefreshEvent extends BookListEvent {}

abstract class BookListState {}

class BookListInitialState extends BookListState {}

class BookListLoadingState extends BookListState {}

class BookListLoadedState extends BookListState {
  final List<Book> books;
  BookListLoadedState(this.books);
}

class BookListErrorState extends BookListState {
  final String error;
  BookListErrorState(this.error);
}

class BookListBloc extends Bloc<BookListEvent, BookListState> {
  final BookRepository bookRepository;
  BookListBloc(this.bookRepository) : super(BookListInitialState());
  @override
  Stream<BookListState> mapEventToState(BookListEvent event) async* {
    if (event is BookListRefreshEvent) {
      yield BookListLoadingState();
      AppResponse<List<Book>> response = await bookRepository.getList();
      if (response.isSuccess) {
        yield BookListLoadedState(response.data);
      } else {
        yield BookListErrorState(response.error);
      }
    }
  }
}
