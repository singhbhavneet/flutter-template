import 'package:template/models/book.dart';

class Routes {
  static const String splash = "/";
  static const String home = "/home";
  static const String book = "/book";
  static const String bookAdd = "/book/add";
}

abstract class RouteData {}

class BookRouteData extends RouteData {
  final Book book;

  BookRouteData(this.book);
}
